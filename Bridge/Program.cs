﻿using System;
using System.Collections.Generic;

namespace Bridge
{
    class Program
    {
        static void Main(string[] args)
        {
            //Client
            TextMaker textMaker = FillTextBuilder(new TextBuilder());
            String text = textMaker.GetText();
            //test: line 1
            // line 2

            TextMaker htmlMaker = FillTextBuilder(new HtmlBuilder());
            String html = htmlMaker.GetText();
            //html: <span>line 1</span><br/>
            // <span>line 2</span><br/>
        }

        static TextMaker FillTextBuilder(TextImp textImp)
        {
            TextMaker textMaker = new TextMaker(textImp);
            textMaker.AddLine("line 1");
            textMaker.AddLine("line 2");
            return textMaker;
        }
    }

    //Abstraction
    abstract class AText
    {
        protected TextImp _textImp;

        //Operations
        public abstract String GetText();
        public abstract void AddLine(String value);
    }

    //RefinedAbstraction
    class TextMaker : AText
    {
        public TextMaker(TextImp textImp)
        {
            _textImp = textImp;
        }

        public override String GetText()
        {
            return _textImp.GetString();
        }

        public override void AddLine(String value)
        {
            _textImp.AppendLine(value);
        }
    }

    //Implementator
    abstract class TextImp
    {
        protected readonly List<string> Rows = new List<string>();

        public String GetString()
        {
            return string.Join("\n", Rows);
        }

        public abstract void AppendLine(String value);
    }

    //ConcreteImplementator1
    class TextBuilder : TextImp
    {

        public override void AppendLine(String value)
        {
            Rows.Add(value);
        }
    }

    //ConcreteImplementator2
    class HtmlBuilder : TextImp
    {

        public override void AppendLine(String value)
        {
            Rows.Add("<span>" + value + "</span><br/>");
        }
    }
}
