﻿using System;

namespace Singleton
{
    class Program
    {
        static void Main(string[] args)
        {
            //Client
            Settings settings = Settings.GetInstance();
            //Settings settings = new Settings(); //<- Error

            settings.Host = "192.168.100.1";
            settings.Port = 33;

            Settings settings1 = Settings.GetInstance();
            //settings1.Port is 33
        }
    }

    //Singleton
    class Settings
    {
        static Settings _instance;

        public static Settings GetInstance()
        {
            return _instance ?? (_instance = new Settings());
        }

        private Settings() { }

        public int Port { get; set; }
        public string Host { get; set; }
    }
}
