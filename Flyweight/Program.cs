﻿using System;
using System.Collections.Generic;

namespace Flyweight
{
    class Program
    {
        static void Main(string[] args)
        {
            //Client
            CharFactory factory = new CharFactory();
            ISpan charA = factory.GetChar('A');
            charA.PrintSpan("font-size: 12");

            ISpan charB = factory.GetChar('B');
            charB.PrintSpan("font-size: 12");

            ISpan charA1 = factory.GetChar('A');
            charA1.PrintSpan("font-size: 12");

            bool equal = charA == charA1;
            //equal is true
        }
    }

    //Flyweight
    interface ISpan
    {
        void PrintSpan(String style);
    }

    //ConcreteFlyweight
    class Char : ISpan
    {
        private readonly char _c;

        public Char(char c)
        {
            _c = c;
        }

        //Operation(extrinsicState)
        public void PrintSpan(String style)
        {
            Console.WriteLine("<span style=\"" +
            style + "\">" + _c + "</span>");
        }
    }

    //FlyweightFactory
    class CharFactory
    {
        readonly Dictionary<char, Char> _chars =
        new Dictionary<char, Char>();

        //GetFlyweight(key)
        public ISpan GetChar(char c)
        {
            Char character;
            if (_chars.ContainsKey(c))
            {
                character = _chars[c];
            }
            else
            {
                character = new Char(c);
                _chars.Add(c, character);
            }
            return character;
        }
    }
}
