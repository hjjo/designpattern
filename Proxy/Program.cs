﻿using System;

namespace Proxy
{
    class Program
    {
        static void Main(string[] args)
        {
            //Client
            var proxy = new ImageProxy("1.png");
            //operation without creating a RealSubject
            var fileName = proxy.GetFileName();
            //forwarded to the RealSubject
            proxy.Draw();
        }
    }

    //Subject
    abstract class Graphic
    {
        protected String FileName;

        public abstract void Draw();

        public String GetFileName()
        {
            return FileName;
        }
    }

    //RealSubject
    class Image : Graphic
    {

        public Image(String fileName)
        {
            FileName = fileName;
        }

        //Request()
        public override void Draw()
        {
            Console.WriteLine("draw " + FileName);
        }
    }

    //Proxy
    class ImageProxy : Graphic
    {
        Image _image;

        public ImageProxy(String fileName)
        {
            FileName = fileName;
        }

        public override void Draw()
        {
            GetImage().Draw();
        }

        private Image GetImage()
        {
            return _image ?? (_image = new Image(FileName));
        }
    }
}
