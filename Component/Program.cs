﻿using System;
using System.Collections.Generic;

namespace Component
{
    class Program
    {
        static void Main(string[] args)
        {
            //Client
            var image = new Image();
            image.Add(new Circle());
            image.Add(new Square());
            var picture = new Image();
            picture.Add(image);
            picture.Add(new Image());
            picture.Draw();
        }
    }

    //Component
    interface IGraphic
    {
        void Draw();
    }

    //Leaf
    class Circle : IGraphic
    {
        public void Draw()
        {
            Console.WriteLine("Draw circle");
        }
    }

    //Leaf
    class Square : IGraphic
    {
        public void Draw()
        {
            Console.WriteLine("Draw square");
        }
    }

    //Composite
    class Image : IGraphic
    {
        readonly List<IGraphic> _graphics =
        new List<IGraphic>();

        public void Add(IGraphic graphic)
        {
            _graphics.Add(graphic);
        }

        public bool Remove(IGraphic graphic)
        {
            return _graphics.Remove(graphic);
        }

        public void Draw()
        {
            foreach (var graphic in _graphics)
            {
                graphic.Draw();
            }
        }
    }
}
