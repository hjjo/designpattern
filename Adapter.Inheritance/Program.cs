﻿using System;
using System.Collections.Generic;

namespace Adapter.Inheritance
{
    class Program
    {
        static void Main(string[] args)
        {
            //Client
            var adapter = GetTextAdapter();
            var text = adapter.GetText();
            //text: line 1
            // line 2
        }

        static TextAdapter GetTextAdapter()
        {
            TextAdapter adapter = new TextAdapter();
            adapter.Add("line 1");
            adapter.Add("line 2");
            return adapter;
        }
    }

    //Target
    interface IText
    {
        //Request()
        String GetText();
    }

    //Adaptee
    class StringList
    {
        readonly List<string> _rows = new List<string>();

        //SpecificRequest()
        public String GetString()
        {
            return string.Join("\n", _rows);
        }

        public void Add(String value)
        {
            _rows.Add(value);
        }
    }

    //Adapter
    class TextAdapter : StringList, IText
    {
        //Request()
        public String GetText()
        {
            return GetString();
        }
    }
}
