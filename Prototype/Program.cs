﻿using System;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            var square = new Square("Red");
            var maker = new ShapeMaker(square);

            var square1 = maker.MakeShape();
            var square2 = (Square)maker.MakeShape();
        }
    }

    //Prototype
    interface IShape
    {
        string Color { get; set; }

        IShape Clone();
    }

    //ConcretePrototype
    class Square : IShape
    {
        public string Color { get; set; }

        public Square(string color)
        {
            Color = color;
        }

        public IShape Clone()
        {
            return new Square(Color);
        }
    }

    //Client
    class ShapeMaker
    {
        private readonly IShape _shape;

        public ShapeMaker(IShape shape)
        {
            _shape = shape;
        }

        public IShape MakeShape()
        {
            return _shape.Clone();
        }
    }
}
