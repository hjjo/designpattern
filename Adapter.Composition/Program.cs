﻿using System;
using System.Collections.Generic;

namespace Adapter.Composition
{
    class Program
    {
        static void Main(string[] args)
        {
            //Client
            var adapter = GetTextAdapter();
            var text = adapter.GetText();
            //text: line 1
            // line 2
        }

        static TextAdapter GetTextAdapter()
        {
            TextAdapter adapter = new TextAdapter();
            StringList rowList = new StringList();
            rowList.Add("line 1");
            rowList.Add("line 2");
            adapter.RowList = rowList;

            return adapter;
        }
    }

    //Target
    interface IText
    {
        //Request()
        String GetText();
    }

    //Adaptee
    class StringList
    {
        readonly List<string> _rows = new List<string>();

        //SpecificRequest()
        public String GetString()
        {
            return string.Join("\n", _rows);
        }

        public void Add(String value)
        {
            _rows.Add(value);
        }
    }

    //Adapter
    class TextAdapter : IText
    {

        public StringList RowList { get; set; }

        //Request()
        public String GetText()
        {
            if (RowList == null)
            {
                return "";
            }
            return RowList.GetString();
        }
    }
}
